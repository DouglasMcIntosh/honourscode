package test;

//import org.freedesktop.*;
import org.freedesktop.dbus.DBusConnection;
import org.freedesktop.dbus.exceptions.DBusException;

public class Main implements Interface{


	public static void main(String[] args){
		try {
			DBusConnection conn = DBusConnection.getConnection(DBusConnection.SYSTEM);
			conn.requestBusName("");
			conn.exportObject("/",new Main());
			
		}catch (DBusException e) {
			System.out.println(e.toString());
		}
	}

	@Override
	public int echoMessage(String str) {
		System.out.println(str);
		return 0;
	}

	@Override
	public int echoAndAdd(String str, int a, int b) {
		System.out.println(str);
		return a+b;
	}
	@Override
	public boolean isRemote() {
		// TODO Auto-generated method stub
		return false;
	}
}
