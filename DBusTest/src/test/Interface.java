package test;

import org.freedesktop.dbus.DBusInterface;

public interface Interface extends DBusInterface{
	
	public int echoMessage(String str);
	
	public int echoAndAdd(String str, int a, int b);
}
