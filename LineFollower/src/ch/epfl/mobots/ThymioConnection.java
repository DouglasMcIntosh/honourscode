package ch.epfl.mobots;

import java.util.ArrayList;
import java.util.List;

import org.freedesktop.dbus.*;
import org.freedesktop.dbus.exceptions.DBusException;
import resources.*;

public class ThymioConnection implements DBusSigHandler 
{
	//Variable assignment
	DBusConnection bus ; //Connection through dbus
	AsebaNetwork network; //network allows reation of Aseba network object in ch.epfl.mobots package
	EventFilter eventFilter; // From mobots package
	String node; // Used to store String of Node from GetNodesList(0)
	private List<String> variables; // list of strings variables.
	
	public ThymioConnection()//Start class, instantiates robot control interface.
	{	
		ClassLoader classLoader = getClass().getClassLoader();
		try 
		{
			//Start dbus connection
			System.out.println("Start of try catch block");
			bus = DBusConnection.getConnection(DBusConnection.SYSTEM);
			System.out.println("bus connection get SYSTEM connection");
			
			//Request bus name
			bus.requestBusName("ch.epfl.mobots");
			System.out.println("bus request name");
			
			//Gets the AsebaNetwork and stores it in the aseba network class.
			network = bus.getRemoteObject("ch.epfl.mobots.Aseba", "/", AsebaNetwork.class);
			System.out.println("System dbus conneciton established");
		} 
		catch (DBusException e) 
		{
			System.err.println("thymio not found - dbus error\n");
			e.printStackTrace();
		}
		
		//Gets list of variables from node 0 and save them to variables list.
		System.out.println("End of try catch, \nGet nodes");
		node = network.GetNodesList().get(0);
		setVariables(network.GetVariablesList(node));
		
		//Loads the event handler script.
		System.out.println("Node established, get script and load to thymio");
		String scriptPath = classLoader.getResource("parallelThymioEvents.aesl").getPath();
		network.LoadScripts(scriptPath);
		System.out.println("loaded .aesl file.");
		
		//Test run of motors
		setRightMotorSpeed((short)100);
		setLeftMotorSpeed((short)100);
		System.out.println("Motorspeeds set");
		
		//Set a time limit on motor movement. To plan corrective movement actions.
		
		
		//try catch block is set up to handle the events.
		try
		{
			System.out.println("Start of event handler try catch");
			bus.addSigHandler(EventFilter.Event.class, new DBusSigHandler<EventFilter.Event>()
			{
				
				@Override
				public void handle(EventFilter.Event sig)
				{
					System.out.println("Mission Accomplished");
				}
			});
		}
		catch (DBusException dBusException)
		{
			System.out.println("Error in signal handler");
			bus.disconnect();
			System.exit(1);
		}
	}
	
	public void handle(DBusSignal signal)
	{
		System.out.println("Signal recieved");
		System.out.println(signal);
		System.out.println(signal.toString());
		System.out.println(signal.getName());
	}
	
	public void loadScript(String filePath) {
		network.LoadScripts(filePath);
	}
	
	public String listVariables() {
		String listOfVariables = "";
		for (String string : getVariables()) {
			listOfVariables += string + "\n";
		}
		System.out.println(listOfVariables);
		return listOfVariables;
	}
	
	public List<Short> getVariable(String variable) {
		List<Short> result = network.GetVariable(node, variable );
		return result;
	}

	public void setVariable(String variable,List<Short> data) {
		network.SetVariable(node, variable,data );
	}

	public void setVariable(String variable,Short data) {
		List<Short> dataList = new ArrayList<>();
		dataList.add(data);
		network.SetVariable(node, variable,dataList );
	}
	
	public void setLeftMotorSpeed(Short speed) {
		setVariable("motor.left.target", speed);
	}

	public void setRightMotorSpeed(Short speed) {
		setVariable("motor.right.target", speed);
	}

	
	public List<Short> getProximitySensor() {
		List<Short> result = network.GetVariable(node, "prox.horizontal");
		return result;
	}
	
	public List<Short> getGroundSensorAmbiant() {
		List<Short> result = network.GetVariable(node, "prox.ground.ambiant");
		return result;
	}

	public List<Short> getGroundSensorReflected() {
		List<Short> result = network.GetVariable(node, "prox.ground.reflected");
		return result;
	}

	public List<Short> getGroundSensorDelta() {
		List<Short> result = network.GetVariable(node, "prox.ground.delta");
		return result;
	}
	
	public List<Short> getMotorLeftSpeed() {
		List<Short> result = network.GetVariable(node, "motor.left.speed");
		return result;
	}

	public List<Short> getMotorRightSpeed() {
		List<Short> result = network.GetVariable(node, "motor.right.speed");
		return result;
	}
	
	public List<Short> getAcc() {
		List<Short> result = network.GetVariable(node, "acc");
		return result;
	}

	public List<Short> getTemperature() {
		List<Short> result = network.GetVariable(node, "temperature");
		return result;
	}
	
	
	
	public void finalize() {
		System.out.println("disconnection");
		bus.disconnect();
	}

	public void setColor(int i, int j, int k) {
		List<Short> data = new ArrayList<>();
		data.add((short)i);
		data.add((short)j);
		data.add((short)k);	
		network.SendEventName("SetColor", data);		
	}

	public void setSpeed(short left, short right) {
		List<Short> data = new ArrayList<>();
		data.add(left);
		data.add(right);
		network.SendEventName("SetSpeed", data);		
	}

	public void setLeftSpeed(short left) {
		List<Short> data = new ArrayList<>();
		data.add(left);
		network.SendEventName("SetLeftSpeed", data);		
	}

	public void setRightSpeed(short right) {
		List<Short> data = new ArrayList<>();
		data.add(right);
		network.SendEventName("SetRightSpeed", data);		
	}

	public List<String> getVariables() {
		return variables;
	}

	public void setVariables(List<String> variables) {
		this.variables = variables;
	}

}
