package ch.epfl.mobots;

import java.util.List;
import org.freedesktop.dbus.*;
import org.freedesktop.dbus.exceptions.DBusException;

public interface EventFilter extends DBusInterface
{
	public void ListenEvent(UInt16 eventId);
	public void ListenEventName(String eventName);
	public void IgnoreEvent(UInt16 eventId);
	public void IgnoreEventName(String eventName); 
	public DBusSignal Event(UInt16 id, String name, List<Short> payloadData);
	
	public class Event extends DBusSignal
	{
		public final UInt16 a;
		public final String str;
		public final List<Short> b;
		
		public Event(String path, UInt16 a, String str, List<Short> b) throws DBusException
		{
			super(path, a, str, b);
			this.a = a;
			this.str = str;
			this.b = b;
			System.out.println("Event created and recieved");
		}
	}
}
