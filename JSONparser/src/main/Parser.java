package main;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class Parser {
	public static void main(String[] args) {
		JSONParser parser = new JSONParser();
		int num = 0;
		
		try {
			
			Object obj = parser.parse(new FileReader("Test.json"));
			JSONObject jsonObj = (JSONObject) obj;
			
			//Object robots = (Object) jsonObj.;
			//System.out.println("ID is: " + id);
			
			//String x = (String) jsonObj.get("x");
			//String y = (String) jsonObj.get("y");
			//System.out.println("Coordinates are: (" + x + "," + y +")");
			
			
			//loop through data
			JSONArray locArray = (JSONArray) jsonObj.get("robots");
			Iterator<Object> iter = locArray.iterator();
			while(iter.hasNext()) {
				
				//System.out.println("Details " + iter.next());
				//String x = (String) jsonObj.get("x");
				//String y = (String) jsonObj.get("y");
				
				JSONObject objNum = (JSONObject) locArray.get(num);
				
				Long id = (Long) objNum.get("id");
				Double x = (Double) objNum.get("x");
				Double y = (Double) objNum.get("y");
				System.out.println("ID is: ("+id+"), Coords (" +x + ","+y + ")" );
				
				num++;
			}
			
		}catch(FileNotFoundException e){e.printStackTrace();}
		catch(IOException e){e.printStackTrace();}
		catch(ParseException e){e.printStackTrace();}
		catch(Exception e){e.printStackTrace();}
		 
	}
	
}
