package jcspNetwork;

import jcsp.lang.*;
import jcsp.net.*;
import jcsp.net.tcpip.*;

public class RunConsumer {
	
	
	public static void main(String[] args) throws Exception{
		
		String rNodeIP = "127.0.0.2";
		TCPIPNodeAddress rNode = new TCPIPNodeAddress(rNodeIP, 3005);
		System.out.println("Check");
		
		Node.getInstance().init (rNode);
		System.out.println("Check - Node address: ");
		System.out.println(rNode.getIpAddress());
		
		NetAltingChannelInput comm = NetChannel.numberedNet2One(100);
		
		new Parallel(
			new CSProcess[]{
				new Consumer(comm)
			}
				
		).run();
		
				
		
	}
	
}

