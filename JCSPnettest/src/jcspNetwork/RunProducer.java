package jcspNetwork;

import jcsp.lang.*;
import jcsp.net.*;
import jcsp.net.tcpip.*;

public class RunProducer {
	
	public static void main(String[] args) throws Exception{
		
		String pNodeIP = "127.0.0.2";
		TCPIPNodeAddress pNode = new TCPIPNodeAddress(pNodeIP, 3005);
		Node.getInstance().init(pNode);
		
		String cNodeIP = "127.0.0.3";
		TCPIPNodeAddress cNode = new TCPIPNodeAddress(cNodeIP, 3005);
		
		NetSharedChannelOutput comm = NetChannel.any2net(cNode, 100);
		
		new Parallel(
				new CSProcess[]{
					new Producer(comm)
				}
					
			).run();
		
	}

}
