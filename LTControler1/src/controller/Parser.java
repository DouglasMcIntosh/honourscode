package controller;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

import org.json.simple.*;

import jcsp.lang.*;

/*
 * The purpose of this process is to process a JSON string supplied by the 
 * previous process in the network. In this case the Stream Reader.
 * 
 * This particular JSON format is in relation to the robotics lab at 
 * napier university and my honours project.
 * 
 * The parser will separate the components utilised in the tracking software and
 * pass them in the format of a pre-defined list of lists
 * 
 * Each element of the list will be in relation to an ID tag that is utilised in
 * the robot lab.
 * 
 * Each sub list, (ID, x, y, yaw) yaw will be used to identify direction of heading.
 */

public class Parser implements CSProcess{
	
	ChannelInput in;
	ChannelOutput out;
	//int num = 0;

	public Parser(ChannelInput in/*, ChannelOutput out*/) {
		/*
		 * Process string of data from JSON reader
		 * 
		 * Instantiate channels.
		 */
		this.in = in;
		this.out = out;
		
	}

	@Override
	public void run() {
		/*
		 * Runs the parser, sets up objects required.
		 */
		JSONParser parser = new JSONParser();
		Object obj;
		JSONObject jsonObj;
		String text = (String) in.read(); //Bin very start of stream as it will contain a partial JSON file.
		
		//Date and time format for logging time stamps.
		String fDate;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		while (true) {
			
			//Read in text from Channel Input and set to text variable.
			text = (String) in.read();
			//System.out.println(text + "<--- data read from channel");//Test stub to confirm data transfer
			
			try {
				//Parse data in string into object then JSON object
				obj = parser.parse(text);
				jsonObj = (JSONObject) obj;
				
				//Collect time stamp date from JSON object then format and display.
				//Comment out in live code for less clutter.
				long utime = (long)jsonObj.get("timestamp");
				Date date = new Date(utime*1000L);
				fDate = sdf.format(date);
				System.out.println("Timestamp: " + fDate);
				
				//Create array of robot data from JSON object.
				JSONArray locArray = (JSONArray) jsonObj.get("robots");
				//System.out.println(locArray.toJSONString()); //Test to confirm data read in to locArray
				
				/*
				 * If there is no data available in the array of robots, do not progress with data transfer
				 * Advise user that no robots have been found.
				 */
				if (locArray.size()==0) {
					System.out.println("Robots array empty, ensure that the light is on in the robot lab, "
							+ "and there are April tags in the Arena.\n"
							+ "Can check: http://evorobots.napier.ac.uk:443/ if necessary.");
					out.write("Turn on lights in lab.");
				}else {
					
					/*
					 * If there are sufficient robots for recording, capture the information and send it on to the controller. 
					 */
					ArrayList<ArrayList<Number>> list = new ArrayList<ArrayList<Number>>();
					
					Iterator<JSONObject> iter = locArray.iterator();
					
					while(iter.hasNext()) {
						
						JSONObject objNum = iter.next();
						//System.out.println("Details " + objNum);
						
						Long id = (Long) objNum.get("id");
						Double x = (Double) objNum.get("x");
						Double y = (Double) objNum.get("y");
						Double yaw = (Double) objNum.get("yaw");
						System.out.println("ID is: ("+id+"), Coords (" +x + ","+y + ": Yaw: " + yaw +")" );//Test of variable allocation.
						
						ArrayList<Number> subList = new ArrayList<Number>();
						subList.add(id);
						subList.add(x);
						subList.add(y);
						subList.add(yaw);
						list.add(subList);
					}
					//out.write(list);
				}
			}
			catch (ParseException e){e.printStackTrace();}
			catch(Exception e){e.printStackTrace();}		
		}
	}
}