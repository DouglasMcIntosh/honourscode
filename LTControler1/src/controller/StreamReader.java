package controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

import jcsp.lang.*;

public class StreamReader implements CSProcess{
	
	ChannelOutput out;
	String jsonIP;
	int jsonPort;

	public StreamReader(ChannelOutput out, String jsonIP, int jsonPort) {
		/*
		 * Create connection to JSON text stream based
		 * on passed values, then send onto Parser
		 */
		this.out = out;
		this.jsonIP = jsonIP;
		this.jsonPort = jsonPort;
	}

	@Override
	public void run() {
		/*
		 * Run program, Read data in from local port ID'd
		 * 
		 * For local port ensure that 
		 * ssh pi@rl-control-01 nc localhost 8080 | nc -l -k -p 8080
		 * has been executede in the command line
		 */
		
		System.out.println("Begin run section of stream reader");
		
		try {
			Socket soc = new Socket(jsonIP, jsonPort);
			BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
			
			String text ="";
			while(true) {
				text = in.readLine();
				//System.out.println("Reader print: "+text);
				out.write(text);
				/*REDACTED!!!
				 * if(text != newText) {
				 * text = newText;
				 * //System.out.println("Reader print: "+text);
				 * out.write(text);
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Stream reader failed");
		}
		
	}

}
