package controller;

/*
 * Imports required for project at various points
 * 
 * JCSP
 * JSON.simple
 * DBus
 * 
 */
import jcsp.lang.*;
import jcsp.net.*;
import jcsp.net.tcpip.TCPIPNodeAddress;



public class RunProgram{
/*
 * Program to control thymio robots at napier university
 * utilising a JSON tracking feed developed by Andreas Steyven
 * 
 * Developed by Douglas McIntosh
 * 20th February 2018
 * 
 */
	public static void main(String[] args) {
		/* Setup variables required for program to execute...
		 * Data for input stream from tracking software.
		 * Need to ensure that JSON stream is being passed
		 * through to the localhost via the following:
		 * 
		 * ssh pi@rl-control-01 nc localhost 8080 | nc -l -k -p 8080 
		 * */
		
		System.out.println("Begin main");
		
		
		/*
		 * Instead of hard coding in the IP addresses utilise chapter 25 
		 * from the fundamentals of parallel systems book. To dynamically
		 * display and connect to the controller.
		 * 
		 * 
		 */
		
		// create a Node and the fromPlayers net channel
		TCPIPNodeAddress nodeAddr = new TCPIPNodeAddress (3005);
		Node.getInstance().init (nodeAddr);
		System.out.println("Controller IP address =" +nodeAddr.getIpAddress());
		
		
		//JSON Stream
		String jsonIP = "127.0.0.1";
		int jsonPort = 8080;
		
		//String pNodeIP = "127.0.0.3";
		//TCPIPNodeAddress pNode = new TCPIPNodeAddress(pNodeIP, 3005);
		//Node.getInstance().init(pNode);
		
		/*
		//Robot IP
		String antIP = "127.0.0.2";
		int antPort = 3005;
		TCPIPNodeAddress antNode = new TCPIPNodeAddress(antIP, antPort);
		//Node.getInstance().init(antNode);
		*/
		

		
		
		System.out.println("Variables set...");
		
		//Channels
		One2OneChannel readToParse = Channel.one2one();
		One2OneChannel parseToNetwork = Channel.one2one();
		//NetChannelOutput comm = NetChannel.any2net(antNode, 100);
		
		System.out.println("Channels set...");
		
		new Parallel(
			new CSProcess[]{
					new StreamReader(readToParse.out(),
							jsonIP,
							jsonPort),
							
					new Parser(readToParse.in())
							
			}
		).run();
		/*
		new Parallel(
			new CSProcess[]{
					new StreamReader(readToParse.out(),
							jsonIP,
							jsonPort),
							
					new Parser(readToParse.in(), 
							parseToNetwork.out()),
							
					new NetworkComm(parseToNetwork.in(), 
							comm),
							
			}
		).run();*/
	}

}
