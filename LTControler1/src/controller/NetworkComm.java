package controller;

import jcsp.lang.*;
import jcsp.net.NetChannelOutput;

public class NetworkComm implements CSProcess{
	
	/*
	 * Process to control interactions between robots and controller
	 * 
	 * In addition this process will handle any communication required
	 * between robots, working as a messenger for robots.
	 * 
	 * I'll need to watch out for potential deadlock issues and perhaps create
	 * more processes as required.
	 */
	
	ChannelInput inLoc;
	ChannelOutput outLoc;

	public NetworkComm(ChannelInput in, NetChannelOutput comm) {
		inLoc = in;
		outLoc = comm;
	}

	public void run() {
		
		while (true){
			String text = inLoc.read().toString();
			System.out.println(text);
			outLoc.write(text);
		}
		
		
		
		
	}

}
