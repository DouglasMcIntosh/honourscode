package netTest;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import jcsp.lang.CSProcess;
import jcsp.lang.ChannelOutput;

public class JSONReader implements CSProcess {
	
	ChannelOutput outChannel;

	public JSONReader(ChannelOutput o) {
		outChannel = o;
	}

	@Override
	public void run() {
		JSONParser parser = new JSONParser();
		int num = 0;
		
		try {
			
			Object obj = parser.parse(new FileReader("Test.json"));
			JSONObject jsonObj = (JSONObject) obj;
			
			//loop through data
			JSONArray locArray = (JSONArray) jsonObj.get("robots");
			Iterator<Object> iter = locArray.iterator();
			
			//As long as there are entries
			while(iter.hasNext()) {
				
				//Get specific object based on num, ranges from 0 - how ever many entries there are.
				JSONObject objNum = (JSONObject) locArray.get(num);
				
				//Capture specifics of the current element.
				Long idl = (Long) objNum.get("id");
				Double id = idl.doubleValue();
				Double x = (Double) objNum.get("x");
				Double y = (Double) objNum.get("y");
				System.out.println("ID is: ("+id+"), Coords (" +x + ","+y + ")" );
				
				ArrayList<Double> ar = new ArrayList();
				ar.add(id);
				ar.add(x);
				ar.add(y);
				
				outChannel.write(ar);
				
				//Iterate to next entry in the json file.
				num++;
			}
			
		}catch(FileNotFoundException e){e.printStackTrace();}
		catch(IOException e){e.printStackTrace();}
		catch(ParseException e){e.printStackTrace();}
		catch(Exception e){e.printStackTrace();}
		
	}
}
