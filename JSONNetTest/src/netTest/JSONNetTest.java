package netTest;

import jcsp.lang.CSProcess;
import jcsp.lang.Parallel;
import jcsp.net.NetChannel;
import jcsp.net.NetSharedChannelOutput;
import jcsp.net.Node;
import jcsp.net.tcpip.TCPIPNodeAddress;

public class JSONNetTest {
	
	public static void main(String[] args) throws Exception{
		
		String pNodeIP = "127.0.0.2";
		TCPIPNodeAddress pNode = new TCPIPNodeAddress(pNodeIP, 3005);
		Node.getInstance().init(pNode);
		
		String cNodeIP = "127.0.0.1";
		TCPIPNodeAddress cNode = new TCPIPNodeAddress(cNodeIP, 3005);
		
		NetSharedChannelOutput comm = NetChannel.any2net(cNode, 100);
		
		new Parallel(
				new CSProcess[]{
					new JSONReader(comm)
				}
					
			).run();
		
	}

}
