package jcspTest;

import jcsp.lang.*;

public class Producer implements CSProcess{

	ChannelOutput outChannel;
	
	public Producer(ChannelOutput o){
		outChannel = o;
	}
	
	public void run() {
		int i = 1000;
		while (i > 0){
			i -= 100;
			outChannel.write(i);		
		}
		
	}

}
