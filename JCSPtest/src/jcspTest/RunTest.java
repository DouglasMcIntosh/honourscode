package jcspTest;

import jcsp.lang.*;

public class RunTest{

	public static void main(String[] args){
		final One2OneChannel a = Channel.one2one();
		
		new Parallel(
			new CSProcess[]{
				new Producer(a.out()),
				new Consumer(a.in())
			}
				
		).run();
		
				
		
	}

}
