package jcspTest;

import jcsp.lang.*;

public class Consumer implements CSProcess{

	ChannelInput inChannel;
	
	public Consumer(ChannelInput i){
		inChannel = i;
	}
	public void run() {
		int i = 1;
		while (i > 0){
			i = (int) inChannel.read();
			System.out.println("value " + i);
		}
		
	}

}
