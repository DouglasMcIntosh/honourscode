package controller;

import jcsp.lang.CSProcess;
import jcsp.lang.Parallel;
import jcsp.net.NetAltingChannelInput;
import jcsp.net.NetChannel;
import jcsp.net.Node;
import jcsp.net.tcpip.TCPIPNodeAddress;

public class Main {

	public static void main(String[] args) throws Exception{
		/*
		 * Read in channel an output to console for proof of concept.
		 * 
		 * :D
		 */

			
			String rNodeIP = "127.0.0.2";
			TCPIPNodeAddress rNode = new TCPIPNodeAddress(rNodeIP, 3005);
			System.out.println("Check");
			
			Node.getInstance().init (rNode);
			System.out.println("Check - Node address: ");
			System.out.println(rNode.getIpAddress());
			
			NetAltingChannelInput comm = NetChannel.numberedNet2One(100);
			
			new Parallel(
				new CSProcess[]{
					new Consumer(comm)
				}
					
			).run();
			
					
			
		}
		
	}