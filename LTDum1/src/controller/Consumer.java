package controller;

import jcsp.lang.CSProcess;
import jcsp.lang.ChannelInput;

public class Consumer implements CSProcess{

	ChannelInput inChannel;
	
	public Consumer(ChannelInput i){
		inChannel = i;
	}
	public void run() {
		
		while (true) {
			String input = (String)inChannel.read();
			System.out.println("Input: " + input);
		}
		
		
	}

}